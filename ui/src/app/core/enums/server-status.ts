export enum ServerStatus {
  ONLINE = "ONLINE",
  OFFLINE = "OFFLINE",
  REBOOTING = "REBOOTING"
}
