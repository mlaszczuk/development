import { Component, Output, EventEmitter, Input } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Server } from 'src/app/core/models/server';

@Component({
  selector: 'app-servers-list',
  templateUrl: './servers-list.component.html',
  styleUrls: ['./servers-list.component.scss']
})
export class ServersListComponent {

  servers: Server[];
  dataSource: MatTableDataSource<Server>;
  displayedColumns: string[] = ['name', 'status', 'dropdown'];

  @Output() turnServerOn = new EventEmitter<Server>();
  @Output() turnServerOff = new EventEmitter<Server>();
  @Output() rebootServer = new EventEmitter<Server>();
  @Input() tableData: MatTableDataSource<Server>;

  turnServerOnClicked(server: Server) {
    this.turnServerOn.emit(server);
  }

  turnServerOffClicked(server: Server) {
    this.turnServerOff.emit(server);
  }

  rebootServerClicked(server: Server) {
    this.rebootServer.emit(server);
  }
}
