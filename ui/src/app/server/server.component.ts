import { Component, OnInit } from '@angular/core';
import { Server } from '../core/models/server';
import { MatTableDataSource } from '@angular/material';
import { ServerService } from '../core/services/server.service';
import { interval, Observable } from 'rxjs';
import { exhaustMap, map, tap, takeUntil, filter, take, first } from 'rxjs/operators';
import { ServerStatus } from '../core/enums/server-status';

@Component({
  selector: 'app-server',
  templateUrl: './server.component.html',
  styleUrls: ['./server.component.scss']
})
export class ServerComponent implements OnInit {

  serverCount: number;
  tableData: MatTableDataSource<Server>;

  constructor(private serverService: ServerService) { }

  ngOnInit() {
    this.serverService.getServerList().subscribe(res => {
      this.tableData = new MatTableDataSource(res);
      this.serverCount = this.tableData.filteredData.length;
    });
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.tableData.filter = filterValue;
  }

  turnServerOn(server: Server) {
    this.serverService.turnServerOn(server).subscribe(s => server.status = s.status);
  }

  turnServerOff(server: Server) {
    this.serverService.turnServerOff(server).subscribe(s => server.status = s.status);
  }

  rebootServer(server: Server) {
    this.serverService.rebootServer(server).subscribe(s => {
      server.status = s.status;
      const source: Observable<number> = interval(1000);
      source.pipe(
        exhaustMap(ss => this.serverService.getServer(server.id)),
        filter(ss => ss.status === ServerStatus.ONLINE),
        first()
      ).subscribe(ss => server.status = ss.status);
    });
  }
}
