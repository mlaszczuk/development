import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ServerRoutingModule } from './server-routing.module';
import { HeaderComponent } from './header/header.component';
import { ServerComponent } from './server.component';
import { ServersListComponent } from './servers-list/servers-list.component';
import {
  MatTableModule,
  MatMenuModule,
  MatIconModule
} from '@angular/material';


@NgModule({
  declarations: [HeaderComponent, ServerComponent, ServersListComponent],
  imports: [
    CommonModule,
    ServerRoutingModule,
    MatTableModule,
    MatMenuModule,
    MatIconModule
  ]
})
export class ServerModule { }
