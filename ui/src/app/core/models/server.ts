import { ServerStatus } from '../enums/server-status';

export interface Server {
  id: number;
  name: string;
  status: ServerStatus;
}
