import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Server } from '../models/server';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})

export class ServerService {

  constructor(private http: HttpClient) { }

  getServerList(): Observable<Server[]> {
    return this.http.get<Server[]>(`${environment.baseUrl}/servers`);
  }

  getServer(serverId: number): Observable<Server> {
    return this.http.get<Server>(`${environment.baseUrl}/servers/${serverId}`);
  }

  turnServerOn(server: Server): Observable<Server> {
    return this.http.put<Server>(`${environment.baseUrl}/servers/${server.id}/on`, server);
  }

  turnServerOff(server: Server): Observable<Server> {
    return this.http.put<Server>(`${environment.baseUrl}/servers/${server.id}/off`, server);
  }

  rebootServer(server: Server): Observable<Server> {
    return this.http.put<Server>(`${environment.baseUrl}/servers/${server.id}/reboot`, server);
  }
}
